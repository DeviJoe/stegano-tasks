import os
import string

import bitarray
from PIL import Image
import argparse


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('IMG_FILE', help="Image file with payload")
    return parser


def normalize_bin(bin_text):
    payload = bin_text[2:]
    zeros = 8 - len(payload)
    payload = '0' * zeros + payload
    return '0b' + payload


if __name__ == '__main__':
    p = get_parser()
    args = p.parse_args()

    msg = ""
    img = Image.open(args.IMG_FILE)

    msg_bits = bitarray.bitarray(1)
    msg_bits = msg_bits[1:]

    width, height = img.size

    for i in range(height):
        for j in range(width):
            test_bit = bitarray.bitarray(1)
            test_bit = test_bit[1:]
            pixel = img.getpixel((j, i))
            pixel_bin = [normalize_bin(bin(pixel[0])), normalize_bin(bin(pixel[1])), normalize_bin(bin(pixel[2]))]
            test_bit.append(int(pixel_bin[0][-1]))
            test_bit.append(int(pixel_bin[1][-1]))
            test_bit.append(int(pixel_bin[2][-2:-1]))
            test_bit.append(int(pixel_bin[2][-1]))

            msg_bits += test_bit

    res = ''
    for i in range(len(msg_bits) // 8):
        try:
            res += msg_bits[i * 8:i * 8 + 8].tobytes().decode()
        except UnicodeDecodeError:
            break

    print(res)
    pass
