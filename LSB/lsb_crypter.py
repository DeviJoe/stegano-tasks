import typing
import argparse
from PIL import Image
import bitarray


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('IMG_FILE', help="Image file with payload")
    parser.add_argument("MESSAGE", help="Payload message")
    parser.add_argument('-o', '--output', help="Output file")
    return parser


def str_to_bit_aray(msg_str: str) -> bitarray.bitarray:
    msg_bits = bitarray.bitarray(1)
    msg_bits.frombytes(msg_str.encode())
    return msg_bits[1:]


def split_bit_array_for_blocks(arr: bitarray.bitarray) -> typing.List[bitarray.bitarray]:
    res = []
    for i in range(len(arr) // 4):
        res.append(arr[i*4:i*4+4])
    return res


def normalize_bin(bin_text):
    payload = bin_text[2:]
    zeros = 8 - len(payload)
    payload = '0' * zeros + payload
    return '0b' + payload


if __name__ == '__main__':
    p = get_parser()
    args = p.parse_args()

    # Взяли изображение
    img = Image.open(args.IMG_FILE)

    # Считали строку в массив битов
    msg_b = str_to_bit_aray(args.MESSAGE)

    # Сделали добивку на случай если текст не кратен 4 (в случае если символ не занимает 1 байт) - перестраховка
    while len(msg_b) % 4 != 0:
        msg_b += 0

    # Побили строку на блоки по 4 бита
    msg_list = split_bit_array_for_blocks(msg_b)

    i = 0
    j = 0
    for elem in msg_list:
        pixel = img.getpixel((i, j))
        pixel_bin = [normalize_bin(bin(pixel[0])), normalize_bin(bin(pixel[1])), normalize_bin(bin(pixel[2]))]
        pixel_bin[0] = pixel_bin[0][:-1] + str(elem[0])
        pixel_bin[1] = pixel_bin[1][:-1] + str(elem[1])
        pixel_bin[2] = pixel_bin[2][:-2] + str(elem[2]) + str(elem[3])
        for k in range(len(pixel_bin)):
            pixel_bin[k] = int(pixel_bin[k][2:], 2)

        img.putpixel((i, j), (*pixel_bin, ))

        width, height = img.size

        if i < width:
            i += 1
        else:
            i = 0
            j += 1

    if args.output:
        img.save(args.output)
        print(f"[ * ] SAVED FILE: {args.output}")
    else:
        img.save(args.IMG_FILE + '.steg.png')
        print(f"[ * ] SAVED FILE: {args.IMG_FILE + '.steg.png'}")
    pass
