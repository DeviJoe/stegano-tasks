import bitarray
import yaml
from PIL import Image
import argparse


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('IMG_FILE', help="Image file with payload")
    return parser


lambda_value = yaml.load(open('app_config.yml'), Loader=yaml.Loader)['KDB']['lambda']
delta = yaml.load(open('app_config.yml'), Loader=yaml.Loader)['KDB']['delta']

if __name__ == '__main__':
    p = get_parser()
    args = p.parse_args()

    msg = ""
    img = Image.open(args.IMG_FILE)

    msg_bits = bitarray.bitarray(1)
    msg_bits = msg_bits[1:]

    width, height = img.size
    # c = 1
    for j in range(delta, height - delta, delta * 2):
        for i in range(delta, width - delta, delta * 2):
            # print(f'[ {i} , {j}] - {c}')
            # c+=1
            try:
                pixel = img.getpixel((i, j))
                blue_sum = 0
                for k in range(1, delta + 1):
                    blue_sum += img.getpixel((i, j + k))[-1] + img.getpixel((i, j - k))[-1] \
                                + img.getpixel((i + k, j))[-1] + img.getpixel((i - k, j))[-1]
                I_blue_star = blue_sum / (4 * delta)

                if pixel[-1] <= int(I_blue_star):
                    msg_bits.append(1)
                else:
                    msg_bits.append(0)
            except IndexError:
                break

    res = ''
    r = ''
    # print(msg_bits)
    for i in range(len(msg_bits) // 8):
        try:
            r = msg_bits[i * 8:i * 8 + 8].tobytes().decode()
        except UnicodeDecodeError:
            break
        res += r
    print(res)
