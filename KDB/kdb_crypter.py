import typing
import argparse

import yaml
from PIL import Image
import bitarray


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('IMG_FILE', help="Image file with payload")
    parser.add_argument("MESSAGE", help="Payload message")
    parser.add_argument('-o', '--output', help="Output file")
    return parser


def str_to_bit_aray(msg_str: str) -> bitarray.bitarray:
    msg_bits = bitarray.bitarray(1)
    msg_bits.frombytes(msg_str.encode())
    return msg_bits[1:]


lambda_value = yaml.load(open('app_config.yml'), Loader=yaml.Loader)['KDB']['lambda']
delta = yaml.load(open('app_config.yml'), Loader=yaml.Loader)['KDB']['delta']

if __name__ == '__main__':
    # Разобрали аргументы
    p = get_parser()
    args = p.parse_args()

    # Взяли изображение
    img = Image.open(args.IMG_FILE)

    # Считали строку в массив битов
    msg_b = str_to_bit_aray(args.MESSAGE)

    i, j = delta, delta

    # c=1
    # print(msg_b)
    for bit in msg_b:
        pixel = img.getpixel((i, j))
        # print(f'[ {i} , {j}] - {c}')
        # c += 1
        L = 0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2]
        if bit == 0:
            blue_color = pixel[2] + L * lambda_value
        else:
            blue_color = pixel[2] - L * lambda_value
        img.putpixel((i, j), (*pixel[:2], int(blue_color)))

        width, height = img.size
        if i < width - delta * 2:
            i += delta * 2
        else:
            i = delta
            j += delta * 2

    if args.output:
        img.save(args.output)
        print(f"[ * ] SAVED FILE: {args.output}")
    else:
        img.save(args.IMG_FILE + '.steg.png')
        print(f"[ * ] SAVED FILE: {args.IMG_FILE + '.steg.png'}")
    pass