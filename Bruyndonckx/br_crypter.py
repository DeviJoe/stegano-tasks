import hashlib
import pickle
import random

import numpy
import yaml
import bitarray
from numpy import diff
from PIL import Image


def str_to_bit_aray(msg_str: str) -> bitarray.bitarray:
    msg_bits = bitarray.bitarray(1)
    msg_bits.frombytes(msg_str.encode())
    return msg_bits[1:]


def img_to_blocks(image: Image.Image, block_size):
    w, h = image.size
    res_blocks = []
    for i in range(w // block_size):
        res_blocks.append([])
        for j in range(h // block_size):
            box = (i * block_size, j * block_size, (i + 1) * block_size, (j + 1) * block_size)
            region = image.crop(box)
            outfile = 'Blocks8x8/{}-{}.png'.format(i, j)
            region.save(outfile, "png")
            res_blocks[i].append(region)
    return res_blocks


def get_pixel_lumen(pixel: tuple):
    return 0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2]


def sort_block_by_lumen(block: Image.Image, a):
    w, h = block.size
    lumenses = []
    lumenses_mask = []

    for i in range(w):
        lumenses_mask.append([])
        for j in range(h):
            lumenses_mask[i].append(False)
            pixel = block.getpixel((i, j))
            pixel_lumen = get_pixel_lumen(pixel)
            lumenses.append((i, j, pixel_lumen))
    lumenses.sort(key=lambda k: k[2])
    # Добавляем еще одну точку, чтобы обработалась крайняя точка
    lumenses.append((None, None, 100))
    x = numpy.array(numpy.arange(w * h + 1))
    dydx = diff([z[2] for z in lumenses]) / diff(x)
    dydx = dydx
    lumenses = lumenses[:-1]

    if numpy.max(dydx) > a:
        for i, px in enumerate(lumenses):
            if dydx[i] > a:
                lumenses_mask[px[0]][px[1]] = True
    else:
        for i, px in enumerate(lumenses):
            if i >= len(lumenses) // 2:
                lumenses_mask[px[0]][px[1]] = True

    return lumenses_mask


def gen_secret_mask(block: Image.Image, steg_key: str):
    res = []
    secret = hashlib.sha1(steg_key.encode()).digest() + pickle.dumps(block)
    r = random.Random(secret)
    w, h = block.size
    for i in range(w):
        res.append([])
        for j in range(h):
            if r.random() < 0.5:
                res[i].append(False)
            else:
                res[i].append(True)
    return res


def find_medium_lumenses_by_masks(block: Image.Image, lumenses_mask, secret_mask):
    w, h = block.size
    # Темный True
    L1A_col = 0
    L1A_sum = 0
    # Темный False
    L1B_col = 0
    L1B_sum = 0
    # Светлый True
    L2A_col = 0
    L2A_sum = 0
    # Светлый False
    L2B_col = 0
    L2B_sum = 0
    # Средняя яркость темной зоны
    L1_col = 0
    L1_sum = 0
    # Средняя яркость светлой зоны
    L2_col = 0
    L2_sum = 0
    for i in range(w):
        for j in range(h):
            pixel = block.getpixel((i, j))
            pixel_lumen = get_pixel_lumen(pixel)

            if secret_mask[i][j] and lumenses_mask[i][j]:
                L1A_sum += pixel_lumen
                L1A_col += 1
                L1_sum += pixel_lumen
                L1_col += 1
            elif not secret_mask[i][j] and lumenses_mask[i][j]:
                L1B_sum += pixel_lumen
                L1B_col += 1
                L1_sum += pixel_lumen
                L1_col += 1
            elif secret_mask[i][j] and not lumenses_mask[i][j]:
                L2A_sum += pixel_lumen
                L2A_col += 1
                L2_sum += pixel_lumen
                L2_col += 1
            else:
                L2B_sum += pixel_lumen
                L2B_col += 1
                L2_sum += pixel_lumen
                L2_col += 1

    result = {
        "darkTrueAvg": L1A_sum / L1A_col,
        "darkFalseAvg": L1B_sum / L1B_col,
        "lightTrueAvg": L2A_sum / L2A_col,
        "lightFalseAvg": L2B_sum / L2B_col,
        "darkTrueCol": L1A_col,
        "darkFalseCol": L1B_col,
        "lightTrueCol": L2A_col,
        "lightFalseCol": L2B_col,
        "lumDark": L1_sum / L1_col,
        "lumLight": L2_sum / L2_col

    }
    return result


def get_average_zone_lumensy(result, m):
    if m == 1:
        l1a = ((result['darkTrueCol'] + result['darkFalseCol']) * result['lumDark']
               + result['darkFalseCol'] * (result['darkTrueAvg'] - result['darkFalseAvg'])) /\
              (result['darkTrueCol'] + result['darkFalseCol'])
        l2a = ((result['lightTrueCol'] + result['lightFalseCol']) * result['lumLight']
               + result['lightFalseCol'] * (result['lightTrueAvg'] - result['lightFalseAvg'])) / \
              (result['lightTrueCol'] + result['lightFalseCol'])
        l1b = ((result['darkTrueCol'] + result['darkFalseCol']) * result['lumDark']
               - result['darkFalseCol'] * (result['darkTrueAvg'] - result['darkFalseAvg'])) / \
              (result['darkTrueCol'] + result['darkFalseCol'])
        l2b = ((result['lightTrueCol'] + result['lightFalseCol']) * result['lumLight']
               - result['lightFalseCol'] * (result['lightTrueAvg'] - result['lightFalseAvg'])) / \
              (result['lightTrueCol'] + result['lightFalseCol'])
    else:
        l1a = ((result['darkTrueCol'] + result['darkFalseCol']) * result['lumDark']
               - result['darkFalseCol'] * (result['darkFalseAvg'] - result['darkTrueAvg'])) / \
              (result['darkTrueCol'] + result['darkFalseCol'])
        l2a = ((result['lightTrueCol'] + result['lightFalseCol']) * result['lumLight']
               - result['lightFalseCol'] * (result['lightTrueAvg'] - result['lightFalseAvg'])) / \
              (result['lightTrueCol'] + result['lightFalseCol'])
        l1b = ((result['darkTrueCol'] + result['darkFalseCol']) * result['lumDark']
               + result['darkFalseCol'] * (result['darkFalseAvg'] - result['darkTrueAvg'])) / \
              (result['darkTrueCol'] + result['darkFalseCol'])
        l2b = ((result['lightTrueCol'] + result['lightFalseCol']) * result['lumLight']
               + result['lightFalseCol'] * (result['lightTrueAvg'] - result['lightFalseAvg'])) / \
              (result['lightTrueCol'] + result['lightFalseCol'])

    return l1a, l2a, l1b, l2b


def update_block_to_avg_lumensy(block: Image.Image, lumensy_mask, secret_mask, l1a, l2a, l1b, l2b):
    w, h = block.size

    for i in range(w):
        for j in range(h):
            pixel = block.getpixel((i, j))
            pixel_lumen = get_pixel_lumen(pixel)
            if lumensy_mask[i][j] and secret_mask[i][j]:
                k = l1a / pixel_lumen
            elif not lumensy_mask[i][j] and secret_mask[i][j]:
                k = l2a / pixel_lumen
            elif lumensy_mask[i][j] and not secret_mask[i][j]:
                k = l1b / pixel_lumen
            else:
                k = l2b / pixel_lumen
            block.putpixel((i, j), (int(pixel[0] * k), int(pixel[1] * k), int(pixel[2] * k)))

    return block


if __name__ == '__main__':
    # Считали изображение
    cfg = yaml.load(open('app_config.yml'), yaml.Loader)
    img = Image.open(cfg['app']['input'])

    # Считали сообщение и перевели в битовый массив
    msg = cfg['app']['message']
    msg_b = str_to_bit_aray(msg)

    # Разбили изображение на матрицу, каждый элемент которой - 8*8 px блоки
    img_blocks = img_to_blocks(img, 8)

    bit_index = 0
    for p, line in enumerate(img_blocks):
        for q, blk in enumerate(line):
            if bit_index < len(msg_b):
                # Яркостная маска блока, где True - это яркий элемент
                lum_mask = sort_block_by_lumen(blk, cfg['app']['alpha'])
                # Секретная маска блока, где True - это элемент B
                sec_mask = gen_secret_mask(blk, cfg['app']['steg_key'])
                # Получили словарь средних яркостей и количеств для формулы
                lumenses_dict = find_medium_lumenses_by_masks(blk, lum_mask, sec_mask)
                # Получили средние яркости для темной и светлой зон с масками секретности
                # и сгенерировали блок с измененной яркостью
                b = update_block_to_avg_lumensy(blk, lum_mask, sec_mask, *get_average_zone_lumensy(lumenses_dict, msg_b[bit_index]))
                # Сохраняем новый блок
                img_blocks[p][q] = b
                bit_index += 1
            else:
                break
            
    res_img = Image.new('RGB', img.size)
    for p, line in enumerate(img_blocks):
        for q, blk in enumerate(line):
            res_img.paste(blk, (p * 8, q * 8))

    res_img.save(cfg['app']['output'])
    pass
