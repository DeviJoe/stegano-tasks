import hashlib
import pickle
import random

import numpy
import yaml
import bitarray
from numpy import diff
from PIL import Image


def str_to_bit_aray(msg_str: str) -> bitarray.bitarray:
    msg_bits = bitarray.bitarray(1)
    msg_bits.frombytes(msg_str.encode())
    return msg_bits[1:]


def img_to_blocks(image: Image.Image, block_size):
    w, h = image.size
    res_blocks = []
    for i in range(w // block_size):
        res_blocks.append([])
        for j in range(h // block_size):
            box = (i * block_size, j * block_size, (i + 1) * block_size, (j + 1) * block_size)
            region = image.crop(box)
            outfile = 'Blocks8x8/{}-{}.png'.format(i, j)
            region.save(outfile, "png")
            res_blocks[i].append(region)
    return res_blocks


def get_pixel_lumen(pixel: tuple):
    return 0.299 * pixel[0] + 0.587 * pixel[1] + 0.114 * pixel[2]


def sort_block_by_lumen(block: Image.Image, a):
    w, h = block.size
    lumenses = []
    lumenses_mask = []

    for i in range(w):
        lumenses_mask.append([])
        for j in range(h):
            lumenses_mask[i].append(False)
            pixel = block.getpixel((i, j))
            pixel_lumen = get_pixel_lumen(pixel)
            lumenses.append((i, j, pixel_lumen))
    lumenses.sort(key=lambda k: k[2])
    # Добавляем еще одну точку, чтобы обработалась крайняя точка
    lumenses.append((None, None, 100))
    x = numpy.array(numpy.arange(w * h + 1))
    dydx = diff([z[2] for z in lumenses]) / diff(x)
    dydx = dydx
    lumenses = lumenses[:-1]

    if numpy.max(dydx) > a:
        for i, px in enumerate(lumenses):
            if dydx[i] > a:
                lumenses_mask[px[0]][px[1]] = True
    else:
        for i, px in enumerate(lumenses):
            if i >= len(lumenses) // 2:
                lumenses_mask[px[0]][px[1]] = True

    return lumenses_mask


def gen_secret_mask(block: Image.Image, steg_key: str):
    res = []
    secret = hashlib.sha1(steg_key.encode()).digest() + pickle.dumps(block)
    r = random.Random(secret)
    w, h = block.size
    for i in range(w):
        res.append([])
        for j in range(h):
            if r.random() < 0.5:
                res[i].append(False)
            else:
                res[i].append(True)
    return res


def find_medium_lumenses_by_masks(block: Image.Image, lumenses_mask, secret_mask):
    w, h = block.size
    # Темный True
    L1A_col = 0
    L1A_sum = 0
    # Темный False
    L1B_col = 0
    L1B_sum = 0
    # Светлый True
    L2A_col = 0
    L2A_sum = 0
    # Светлый False
    L2B_col = 0
    L2B_sum = 0
    # Средняя яркость темной зоны
    L1_col = 0
    L1_sum = 0
    # Средняя яркость светлой зоны
    L2_col = 0
    L2_sum = 0
    for i in range(w):
        for j in range(h):
            pixel = block.getpixel((i, j))
            pixel_lumen = get_pixel_lumen(pixel)

            if secret_mask[i][j] and lumenses_mask[i][j]:
                L1A_sum += pixel_lumen
                L1A_col += 1
                L1_sum += pixel_lumen
                L1_col += 1
            elif not secret_mask[i][j] and lumenses_mask[i][j]:
                L1B_sum += pixel_lumen
                L1B_col += 1
                L1_sum += pixel_lumen
                L1_col += 1
            elif secret_mask[i][j] and not lumenses_mask[i][j]:
                L2A_sum += pixel_lumen
                L2A_col += 1
                L2_sum += pixel_lumen
                L2_col += 1
            else:
                L2B_sum += pixel_lumen
                L2B_col += 1
                L2_sum += pixel_lumen
                L2_col += 1

    if L1A_sum / L1A_col >= L2A_sum / L2A_col:
        print('e')
    if L1B_sum / L1B_col >= L2B_sum / L2B_col:
        print('k')
    result = {
        "darkTrueAvg": L1A_sum / L1A_col,
        "darkFalseAvg": L1B_sum / L1B_col,
        "lightTrueAvg": L2A_sum / L2A_col,
        "lightFalseAvg": L2B_sum / L2B_col,
        "darkTrueCol": L1A_col,
        "darkFalseCol": L1B_col,
        "lightTrueCol": L2A_col,
        "lightFalseCol": L2B_col,
        "lumDark": L1_sum / L1_col,
        "lumLight": L2_sum / L2_col

    }
    return result


def get_bit_by_lum(result: dict):
    res_bit = None
    if result['darkTrueAvg'] - result['darkFalseAvg'] < 0 and result['lightTrueAvg'] - result['lightFalseAvg'] < 0:
        res_bit = 0
    elif result['darkTrueAvg'] - result['darkFalseAvg'] > 0 and result['lightTrueAvg'] - result['lightFalseAvg'] > 0:
        res_bit = 1
    return res_bit


if __name__ == '__main__':
    # Считали изображение
    cfg = yaml.load(open('app_config.yml'), yaml.Loader)
    img = Image.open(cfg['app']['output'])

    # Разбили изображение на матрицу, каждый элемент которой - 8*8 px блоки
    img_blocks = img_to_blocks(img, 8)

    # Подготовили контейнер для сообщения
    msg_bits = bitarray.bitarray(1)
    msg_bits = msg_bits[1:]

    for p, line in enumerate(img_blocks):
        for q, blk in enumerate(line):
            # Яркостная маска блока, где True - это яркий элемент
            lum_mask = sort_block_by_lumen(blk, cfg['app']['alpha'])
            # Секретная маска блока, где True - это элемент B
            sec_mask = gen_secret_mask(blk, cfg['app']['steg_key'])
            # Получили словарь средних яркостей и количеств для формулы
            lumenses_dict = find_medium_lumenses_by_masks(blk, lum_mask, sec_mask)
            bit = get_bit_by_lum(lumenses_dict)

            if bit is None:
                continue

            msg_bits.append(bit)

    print(msg_bits)
    msg = cfg['app']['message']
    msg_b = str_to_bit_aray(msg)
    print(msg_b)

    res = ''
    r = ''

    for i in range(len(msg_bits) // 8):
        try:
            r = msg_bits[i * 8:i * 8 + 8].tobytes().decode()
        except UnicodeDecodeError:
            break
        res += r
    print(res)